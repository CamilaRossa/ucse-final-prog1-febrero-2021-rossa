FINAL-PROG1-FEBRERO2021-ROSSACAMILA
1)
	A)FALSO. Se debe utilizar ".Count", ya que "Lenght" no se permite para las listas.
	B)VERDADERO.
	C)VERDADERO.
	D)FALSO. No se tendría que llevar un contador. Se debería ir dividiendo el item. En el caso que muestre si tengo el contador en 1 pero el número es un 4, la operación "contador%2" va a dar 1 y sumaría el 4, es decir, un número par a los impares.

2)proximaAccion = opcion == “Carga” ? “CARGAR_OPCIONES” : opcion == “Edicion” || opcion = “Eliminacion” ? BUSCAR_OPCION_POR_ID : RETORNAR_ERROR_OPCION;

3)	public bool Iguales(ref resultado)
	{
		return iguales;
	}

4)El objetivo de usar interfaces es que para cuando se tenga más de una clase hagan lo mismo. Usar interfaces permite realizar polimorfismo y agregar nuevas clases de forma mucho más fácil. 
Sería lo mismo tener 2 clases que hagan lo mismo pero de diferente manera, lo que cambia es que con las interfaces varía la implementación. Es por esto que en programación orientada a objetos decimos que las interfaces son funcionalidades (o comportamientos) y las clases representan implementaciones.
 
5) -parrafo: variable a nivel de clase.
-cantidad: variable a nivel de método.
-palabra: variable a nivel de bloque.
-letra: variable a nivel de bloque.

6) -línea 2: hay un ":" en lugar de ";".
-línea 3: no se puede asignar "null" a la variable edad porque no está definida.
-línea 5: para calcular la edad debería ser: "DateTime.Today.AddTicks(-fechaNac.Ticks).Year-1;"

7)p1,p2 son parámetros reales. Es decir que son variables concretas utilizadas para la invocación del método: "ObtenerDistancia".
point1,point2 son parámetros formales. Es decir, parámetros definidos en el método, donde serán utilizados para la lógica del mismo.