﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOGICA
{
    public abstract class Juego
    {
        public int Codigo { get; set; }
        public double Largo { get; set; }
        public double Ancho { get; set; }
        public int EdadDesde { get; set; }
        public int EdadHasta { get; set; }
        public double PrecioBase { get; set; }

        public virtual string ObtenerDescripcion()
        {
            return $"Codigo: {Codigo} - Tamaño: {Largo}x{Ancho} - ";
        }
        public abstract int CalcularFechaEstimada();
    }
}
