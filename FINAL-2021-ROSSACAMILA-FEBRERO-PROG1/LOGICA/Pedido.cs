﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOGICA
{
    public class Pedido
    {
        public int NumeroPedido { get; set; }
        public int DniCliente { get; set; }
        public DateTime FechaCreacionPedido { get; set; }
        public DateTime FechaEntregaEstimada { get; set; }
        public double CostoTotal { get; set; }
        public EFormaPago FormaPago { get; set; }
        public DateTime FechaPago { get; set; }

        public enum EFormaPago
        {
            efectivo,transferencia,mercadoPago
        }
    }
}
