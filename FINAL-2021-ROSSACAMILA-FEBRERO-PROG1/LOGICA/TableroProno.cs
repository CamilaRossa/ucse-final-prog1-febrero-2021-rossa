﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOGICA
{
    public class TableroProno:Juego
    {
        public int CantidadBarras { get; set; }
        public bool DiseñoPersonalizado { get; set; }
        public List<string> ListadoColores { get; set; }

        public override int CalcularFechaEstimada()
        {
            int cantidad = 0;
            if (CantidadBarras<=3)
            {
                cantidad = 10;
            }
            else
            {
                cantidad = 15;
            }
            if (ListadoColores.Count==3)
            {
                cantidad += 2;
            }
            return cantidad;
        }

        public override string ObtenerDescripcion()
        {
            return $"Tipo: Prono - {base.ObtenerDescripcion()} CantidadBarras: {CantidadBarras} - EsPersonalizado: {DiseñoPersonalizado} ";
        }
    }
}
