﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOGICA
{
    public class Cliente
    {
        public int DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int CP { get; set; }
        public string Domicilio { get; set; }
        public string Correo { get; set; }
        public int Telef { get; set; }
    }
}
