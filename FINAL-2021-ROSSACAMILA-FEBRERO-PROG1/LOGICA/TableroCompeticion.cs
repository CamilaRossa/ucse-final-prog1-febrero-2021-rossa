﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOGICA
{
    public class TableroCompeticion:Juego
    {
        public string ColorTablero { get; set; }
        public ECategoria Categoria { get; set; }
        public int CantidadJugadores { get; set; }

        public enum ECategoria
        {
            deporte,didactico,estrategia
        }
        public override string ObtenerDescripcion()
        {
            return $"Tipo: Competicion {base.ObtenerDescripcion()} -CantidadJugadores: {CantidadJugadores} - Categoria: {Categoria}";
        }

        public override int CalcularFechaEstimada()
        {
            if (Categoria==TableroCompeticion.ECategoria.estrategia)
            {
                return 10;
            }
            return 15;
        }
    }
}
