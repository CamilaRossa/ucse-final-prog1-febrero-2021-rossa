﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOGICA
{
    public class Principal
    {
        List<Cliente> ListaCliente = new List<Cliente>();
        List<Juego> ListaJuego = new List<Juego>();
        List<Pedido> ListaPedido = new List<Pedido>();

        public Resultado DarAltaNuevoCliente(Cliente nuevoCliente)
        {
            Resultado nuevoResultado = new Resultado();
            if (ListaCliente.Exists(x => x.DNI == nuevoCliente.DNI))
            {
                nuevoResultado.Mensaje = "Error, el cliente ya existe";
                nuevoResultado.ResultadoOperacion = false;
            }
            else
            {
                if (nuevoCliente.Nombre==null || nuevoCliente.Domicilio==null || nuevoCliente.Telef==0)
                {
                    nuevoResultado.Mensaje = "Error, datos incompletos";
                    nuevoResultado.ResultadoOperacion = false;
                }
                else
                {
                    nuevoResultado.Mensaje = "Se dio de alta nuevo cliente";
                    nuevoResultado.ResultadoOperacion = true;
                    ListaCliente.Add(nuevoCliente);
                }
            }
            return nuevoResultado;
        }

        public List<string> DevolverListadoTablero()
        {
            List<string> DevolverLista = new List<string>();
            foreach (var item in ListaJuego)
            {
                DevolverLista.Add(item.ObtenerDescripcion());
            }
            return DevolverLista;
        }

        public void DarAltaNuevoPedido(int dni, int numeroTablero)
        {
            Cliente clienteEncontrado = ListaCliente.Find(x => x.DNI == dni);
            Juego tableroEncontrado = ListaJuego.Find(x => x.Codigo == numeroTablero);

            Pedido nuevoPedido = new Pedido();
            nuevoPedido.DniCliente = dni;
            nuevoPedido.FechaCreacionPedido = DateTime.Today;
            nuevoPedido.NumeroPedido = ListaPedido.Count + 1;
            nuevoPedido.FechaEntregaEstimada.AddDays(tableroEncontrado.CalcularFechaEstimada());
            nuevoPedido.CostoTotal = tableroEncontrado.PrecioBase + (tableroEncontrado.CalcularFechaEstimada() * 80);
            ListaPedido.Add(nuevoPedido);
        }

        public void ActualizarEstadoPedido(int numeroPedido, EFormaPago formaPago)
        {
            ListaPedido[ListaPedido.FindIndex(x => x.NumeroPedido == numeroPedido)].FormaPago = formaPago;
            ListaPedido[ListaPedido.FindIndex(x => x.NumeroPedido == numeroPedido)].FechaPago = DateTime.Today;
        }

    }
}
