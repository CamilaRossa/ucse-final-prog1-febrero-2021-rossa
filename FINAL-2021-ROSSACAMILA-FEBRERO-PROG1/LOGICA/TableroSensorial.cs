﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOGICA
{
    public class TableroSensorial:Juego
    {
        public int CantidadJuegos { get; set; }
        public string ColorTablero { get; set; }
        public bool DiseñoPersonalizado { get; set; }

        public override int CalcularFechaEstimada()
        {
            if (DiseñoPersonalizado)
            {
                return 14;
            }
            return 10;
        }

        public override string ObtenerDescripcion()
        {
            return $" Tipo: Sensorial - {base.ObtenerDescripcion()} - CantidadJuegos: {CantidadJuegos} - Es personalizado: {DiseñoPersonalizado}";
        }
    }
}
